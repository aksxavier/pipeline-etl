import os
import pandas as pd
from pyspark.sql.functions import col, expr

from exec_etl_to_csv import Execucao

os.environ['JAVA_HOME'] = "C:\Program Files\Java\jdk1.8.0_331"
os.environ['SPARK_HOME'] = "C:\Spark\spark-3.2.1-bin-hadoop3.2"

import findspark
findspark.init('C:\Spark\spark-3.2.1-bin-hadoop3.2')

from pyspark.sql import SparkSession

spark = SparkSession.builder.getOrCreate()

teste = Execucao()

df_infomix = spark.read.csv('data_etl_transform/infomix.csv', header=True)
df_infomix.createOrReplaceTempView('infomix')

df_gs1 = spark.read.csv('data_etl_transform/gs1.csv', header=True)
df_gs1.createOrReplaceTempView('gs1')

df_cnpjs_receita_federal = spark.read.csv('data_etl_transform/cnpjs_receita_federal.csv', header=True)
df_cnpjs_receita_federal.createOrReplaceTempView('cnpjs_receita_federal')

df_descricoes_externas = spark.read.csv('data_etl_transform/descricoes_externas.csv', header=True)
df_descricoes_externas.createOrReplaceTempView('descricoes_externas')

df_cosmos = spark.read.csv('data_etl_transform/cosmos.csv', header=True)
df_cosmos.createOrReplaceTempView('cosmos')

select_infomix = spark.sql("""SELECT g.id_gs1, g.gtin_gs1, g.status_gs1, i.cnpj_infomix, 
                            i.gtin_infomix, i.category FROM infomix i JOIN gs1 g ON 
                            i.gtin_infomix = g.gtin_gs1 WHERE g.status_gs1 NOT LIKE '%ERROR%'""")


select_infomix.createOrReplaceTempView('infomix_gs1')

select_cnpj_receita = spark.sql("""SELECT c.id_receita_federal, c.cnpj_receita_federal, c.status_receita_federal, 
                                c.created_at, c.fantasia, c.uf, c.municipio, ig.id_gs1, ig.gtin_gs1, ig.status_gs1, 
                                ig.cnpj_infomix, ig.gtin_infomix, ig.category FROM cnpjs_receita_federal c JOIN 
                                infomix_gs1 ig ON c.cnpj_receita_federal = ig.cnpj_infomix WHERE c.status_receita_federal NOT LIKE '%ERROR%'""")


select_cosmos = spark.sql("""SELECT de.id_descricoes_externas, de.gtin_descricoes_externas, de.description,
                             de.flag_infoprice, co.id_cosmos, co.gtin_cosmos, co.description_cosmos 
                             FROM cosmos co JOIN descricoes_externas de ON de.gtin_descricoes_externas 
                             = co.gtin_cosmos WHERE de.flag_infoprice = 'true'""")



df_select_cosmos = select_cosmos.select(col('*'), expr('CASE WHEN description_cosmos IS NULL THEN description ELSE description_cosmos END').alias('new_description'))


df_select_cosmos.createOrReplaceTempView('cosmos_final')
select_cnpj_receita.createOrReplaceTempView('receita_infomix_gs1')

final_dataset = spark.sql("SELECT r.gtin_gs1, r.cnpj_receita_federal, r.fantasia, r.municipio, r.uf, c.new_description, r.category FROM receita_infomix_gs1 r JOIN cosmos_final c ON r.gtin_gs1 = c.gtin_cosmos")

data = final_dataset.toPandas()
df = pd.DataFrame(data = data)
df.to_csv('data_etl_transform/final_dataset_etl.csv', header=['GTIN', 'CNPJ', 'RAZAO SOCIAL', 'CIDADE', 'ESTADO', 'DESCRICAO', 'CATEGORIA'], index=False)