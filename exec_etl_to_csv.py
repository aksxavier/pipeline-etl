
import function_etl_to_csv
import pandas as pd
import json

class Execucao():
    # CHAMADA DA FUNCAO load_table_info_mix
    lst_infomix = function_etl_to_csv.load_table_info_mix("data_etl_test/infomix.tsv")
    df = pd.DataFrame(lst_infomix)
    df.to_csv('data_etl_transform/infomix.csv', header=['cnpj_infomix', 'gtin_infomix', 'category'], index=False)

    # CHAMADA DA FUNCAO load_table_descricoes_externas
    lst_descricoes_externas = function_etl_to_csv.load_table_descricoes_externas("data_etl_test/descricoes_externas.tsv")
    df = pd.DataFrame(lst_descricoes_externas)
    df.to_csv('data_etl_transform/descricoes_externas.csv', header=['id_descricoes_externas', 'gtin_descricoes_externas', 'description', 'flag_infoprice'], index=False)


    # ARQUIVO .JL cnpjs_receita_federal PARA .CSV
    data = [json.loads(line) for line in open('data_etl_test/cnpjs_receita_federal.jl', 'r')]

    datas = []
    for i in range(len(data)):
        try:
            id = data[i]['id']
            cnpj = data[i]['cnpj']
            created_at = data[i]['created_at']
            status = data[i]['response']['status']
            uf = data[i]['response']['uf']
            municipio = data[i]['response']['municipio']
            fantasia = data[i]['response']['fantasia']
        except:
            exceptions_cnpj_receita_federal = open('data_etl_transform/exceptions/cnpjs_receita_federal_file.txt', 'a')
            exceptions_cnpj_receita_federal.write(id + '\n')
            exceptions_cnpj_receita_federal.close()
        d = {'id': id, 'cnpj': cnpj, 'status': status, 'created_at': created_at, 'fantasia':fantasia, 'uf':uf, 'municipio':municipio}
        datas.append(d)

    df = pd.DataFrame(data=datas)
    df.to_csv('data_etl_transform/cnpjs_receita_federal.csv', header=['id_receita_federal', 'cnpj_receita_federal', 'status_receita_federal', 'created_at', 'fantasia', 'uf', 'municipio'], index=False)


    # ARQUIVO .JL gs1 PARA .CSV
    data = [json.loads(line) for line in open('data_etl_test/gs1.jl', 'r')]

    datas = []
    for i in range(len(data)):
        id = data[i]['id']
        gtin = data[i]['gtin']
        status = data[i]['response']['status']

        d = {'id':id, 'gtin':gtin, 'status':status}
        datas.append(d)
        
    df = pd.DataFrame(data=datas)
    df.to_csv('data_etl_transform/gs1.csv', header=['id_gs1', 'gtin_gs1', 'status_gs1'], index=False)


    # ARQUIVO .JL cosmos PARA .CSV
    data = [json.loads(line) for line in open('data_etl_test/cosmos.jl', 'r')]

    datas = []
    for i in range(len(data)):
        try:
            id = data[i]['id']
            gtin = data[i]['gtin']
            description = data[i]['response']['description']
        except:
            exceptions_cosmos = open('data_etl_transform/exceptions/cosmos.txt', 'a')
            exceptions_cosmos.write(id + '\n')
            exceptions_cosmos.close()
        d = {'id':id, 'gtin':gtin, 'description':description}
        datas.append(d)

    df = pd.DataFrame(data=datas)
    df.to_csv('data_etl_transform/cosmos.csv', header=['id_cosmos', 'gtin_cosmos', 'description_cosmos'], index=False)