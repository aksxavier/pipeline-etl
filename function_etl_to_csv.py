import pandas as pd

#ARQUIVO .TSV infomix PARA .CSV
def load_table_info_mix(fname):
    lst = []
    with open(fname, encoding='utf8') as f:
        for line in f:
            parts = line.rstrip().split('\t')
            cnpj = str(parts[0])
            gtin = str(parts[1])
            category = parts[2]
            tup = (cnpj, gtin, category)
            lst.append(tup)
    return lst


#ARQUIVO .TSV descricoes_externas PARA .CSV
def load_table_descricoes_externas(fname):
    lst = []
    with open(fname, encoding='utf8') as f:
        for line in f:
            parts = line.rstrip().split('\t')
            id = parts[0]
            gtin = parts[1]
            description = parts[2]
            flag_infoprice = parts[3]
            tup = (id, gtin, description, flag_infoprice)
            lst.append(tup)
    return lst